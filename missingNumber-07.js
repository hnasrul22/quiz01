const getMissingNumber = (arr) => {

    // Sort the array by asc
    const sort = arr.sort();

    // Make a miss array to save the miss number
    const miss = [];

    // Looping the sort array
    sort.forEach((item,index,array) => {
        if(index < array.length - 1){

            // If the item+1 not same with the next item will push that number in miss array
            if(item+1 != array[index + 1]) {
                miss.push(item + 1)

                /**
                 * It will check if the number that miss is more than one continually
                 * check the miss is not null and the last miss is not the next item in array
                 */
                while(miss.length != 0 && miss[miss.length-1] != array[index + 1]){
                    miss.push(miss[miss.length-1]+1);
                }

                // Will remove the last of while scope, cause the last number is the next of item in array
                miss.splice(-1)
            }
        }
    });

    return miss.join()
}

module.exports = getMissingNumber