const isRepeatDigits = (num) => {

    // Number must contain more than 1 digit
    if(num < 10) return false

    // Convert to string and split it to make an array
    const arr = num.toString().split('')

    // By default is a repeatDigits
    let isRepeat = true

    // Make a looping to compare the value one by one
    arr.forEach((item,index,arr)=>{

        /**
         * The last comparing is 2 ends number, 
         * cause the last digit cannot compare with next digit
         *  */ 
        if(index != arr.length-1)
            // if the digit not equals as next digit, will return false directly
            if(item != arr[index + 1]) {
                isRepeat = false
            }
    })

    return isRepeat
}

module.exports =  isRepeatDigits