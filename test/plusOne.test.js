const plusOne = require('../plusOne-09')

test('09. Return the following number from an integer passed',()=>{
    expect(plusOne(9)).toBe(10);
    expect(plusOne(21)).toBe(22);
    expect(plusOne(26)).toBe(27);
})