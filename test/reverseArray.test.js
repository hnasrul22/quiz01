const reverse = require('../reverseArray-08')

test('08. Reversing arrays',()=>{
    expect(reverse([2,1,7,4,3])).toStrictEqual([3,4,7,1,2])
    expect(reverse(['Deni','Kiki','Roni'])).toStrictEqual(['Roni','Kiki','Deni'])
    expect(reverse(['Rudi',2,'Budi',3])).toStrictEqual([3,'Budi',2,'Rudi'])
})