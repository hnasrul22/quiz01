const sumPrime = require('../sumPrime-03');

test('03. Calculate the sum of prime numbers',()=>{
    expect(sumPrime([1,2,3,4,5])).toBe(10);
    expect(sumPrime([11,24,23,21,3])).toBe(37);
    expect(sumPrime([7,3,9,11,3])).toBe(24);
})