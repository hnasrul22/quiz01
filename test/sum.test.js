const sum = require('../sum-10')

test('10. Return the sum of two numbers',()=>{
    expect(sum(1,2)).toBe(3)
    expect(sum(8,10)).toBe(18)
    expect(sum(11,22)).toBe(33)
})